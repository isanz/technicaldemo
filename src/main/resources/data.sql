-- brands
INSERT INTO brands (id, description) VALUES (1, 'Zara');
INSERT INTO brands (id, description) VALUES (2, 'Bershka');
INSERT INTO brands (id, description) VALUES (3, 'Massimo Dutti');
INSERT INTO brands (id, description) VALUES (4, 'Stradivarius');

--price rates
INSERT INTO price_rates (id, name) VALUES (1, 'Normal');
INSERT INTO price_rates (id, name) VALUES (2, 'Black Friday');
INSERT INTO price_rates (id, name) VALUES (3, 'New Collection');
INSERT INTO price_rates (id, name) VALUES (4, 'Christmas');

--products
INSERT INTO products (id, name, description) VALUES (1, 'Skirt', 'Blue denim skirt');
INSERT INTO products (id, name, description) VALUES (2, 'Shorts', 'Printed shorts');
INSERT INTO products (id, name, description) VALUES (35455, 'Hat', 'Straw hat');

--prices
INSERT INTO prices (id, brand_id, start_date, end_date, price_rate_id, product_id, priority, price, currency) VALUES (1, 1, '2020-06-14 00:00:00', '2020-12-31 23:59:59', 1, 35455, 0, 35.50, 'EUR');
INSERT INTO prices (id, brand_id, start_date, end_date, price_rate_id, product_id, priority, price, currency) VALUES (2, 1, '2020-06-14 15:00:00', '2020-06-14 18:30:00', 2, 35455, 1, 25.45, 'EUR');
INSERT INTO prices (id, brand_id, start_date, end_date, price_rate_id, product_id, priority, price, currency) VALUES (3, 1, '2020-06-15 00:00:00', '2020-06-15 11:00:00', 3, 35455, 1, 30.50, 'EUR');
INSERT INTO prices (id, brand_id, start_date, end_date, price_rate_id, product_id, priority, price, currency) VALUES (4, 1, '2020-06-15 16:00:00', '2020-12-31 23:59:59', 4, 35455, 1, 38.95, 'EUR');



