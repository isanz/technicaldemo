package com.amaris.demo.controller;

import com.amaris.demo.domain.ProductPrice;
import com.amaris.demo.service.PriceService;
import java.time.LocalDateTime;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Validated
@RestController
@RequestMapping(value = PriceController.PRICE_RESOURCE)
public class PriceController {

    public static final String PRICE_RESOURCE = "/price";
    public static final String REQUEST_PARAM_PRODUCT_ID = "productId";
    public static final String REQUEST_PARAM_BRAND_ID = "brandId";
    public static final String REQUEST_PARAM_APPLICATION_DATE = "date";

    private final PriceService priceService;

    public PriceController(PriceService priceService) {
        this.priceService = priceService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ProductPrice searchPrices(@RequestParam(value = REQUEST_PARAM_PRODUCT_ID) @NotNull @Positive Long productId,
        @RequestParam(value = REQUEST_PARAM_BRAND_ID) @NotNull @Positive Long brandId,
        @RequestParam(value = REQUEST_PARAM_APPLICATION_DATE)
        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) @NotNull final LocalDateTime date) {

        return priceService.searchPrices(productId, brandId, date);
    }

}
