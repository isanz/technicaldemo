package com.amaris.demo.domain;

import java.time.LocalDateTime;
import lombok.Data;

@Data
public class ProductPrice {

    private Long productId;

    private Long priceRateId;

    private Long brandId;

    private LocalDateTime startDate;

    private LocalDateTime endDate;

    private String price;

    private String currency;

}
