package com.amaris.demo.service;

import com.amaris.demo.domain.ProductPrice;
import com.amaris.demo.exception.PriceNotFoundException;
import com.amaris.demo.mapper.PriceMapper;
import com.amaris.demo.model.Price;
import com.amaris.demo.repository.PriceRepository;
import java.time.LocalDateTime;
import java.util.Comparator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@Transactional
public class PriceService {

    private final PriceRepository priceRepository;
    private final PriceMapper priceMapper;

    public PriceService(PriceRepository priceRepository, PriceMapper priceMapper) {
        this.priceRepository = priceRepository;
        this.priceMapper = priceMapper;
    }

    public ProductPrice searchPrices(Long productId, Long brandId, LocalDateTime applicationDate) {
        log.info(String.format("Searching by productId: %s, brandId: %s and date: %s", productId, brandId, applicationDate));
        return priceRepository.findByProductIdAndBrandIdAndStartDateLessThanEqualAndEndDateGreaterThanEqual(
                productId, brandId, applicationDate, applicationDate).stream()
            .max(Comparator.comparing(Price::getPriority))
            .map(priceMapper::productPriceMap)
            .orElseThrow(PriceNotFoundException::new);
    }

}
