package com.amaris.demo.exception;

import com.amaris.demo.domain.ErrorResponse;
import javax.validation.ConstraintViolationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

@Slf4j
@ControllerAdvice
public class PriceExceptionHandler {

    public static final String PRICE_NOT_FOUND_ERROR_CODE = "PRICE_NOT_FOUND";
    public static final String INVALID_REQUEST_PARAMS_CODE = "INVALID_PARAMS";
    public static final String GENERIC_ERROR = "INTERNAL_ERROR";
    public static final String PRICE_NOT_FOUND_ERROR_MESSAGE = "The requested resource was not found";
    public static final String INVALID_REQUEST_PARAMS_MESSAGE = "Param %s is not valid";
    public static final String GENERIC_ERROR_ERROR_MESSAGE = "An error occurred while executing the operation";

    @ExceptionHandler({PriceNotFoundException.class})
    @ResponseBody
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public ErrorResponse processPriceNotFound() {
        log.error("Resource not found");
        return new ErrorResponse(PRICE_NOT_FOUND_ERROR_CODE, PRICE_NOT_FOUND_ERROR_MESSAGE);
    }

    @ExceptionHandler({MethodArgumentTypeMismatchException.class})
    @ResponseBody
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorResponse processBadRequest(MethodArgumentTypeMismatchException ex) {
        log.error(String.format("Invalid request parameter: %s", ex.getName()));
        return new ErrorResponse(INVALID_REQUEST_PARAMS_CODE, String.format(INVALID_REQUEST_PARAMS_MESSAGE, ex.getName()));
    }

    @ExceptionHandler({ConstraintViolationException.class})
    @ResponseBody
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorResponse processConstraintViolation(ConstraintViolationException ex) {
        log.error(String.format("Invalid request parameter: %s", ex.getMessage()));
        return new ErrorResponse(INVALID_REQUEST_PARAMS_CODE, String.format(INVALID_REQUEST_PARAMS_MESSAGE, ex.getMessage()));
    }

    @ExceptionHandler({Exception.class})
    @ResponseBody
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse processGenericError(Exception ex) {
        log.error(String.format("Internal server error: %s", ex.getMessage()));
        return new ErrorResponse(GENERIC_ERROR, GENERIC_ERROR_ERROR_MESSAGE);
    }

}
