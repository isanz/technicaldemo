package com.amaris.demo.mapper;

import com.amaris.demo.domain.ProductPrice;
import com.amaris.demo.model.Price;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface PriceMapper {

    @Mapping(source = "brand.id", target = "brandId")
    @Mapping(source = "product.id", target = "productId")
    @Mapping(source = "priceRate.id", target = "priceRateId")
    ProductPrice productPriceMap(Price price);

}
