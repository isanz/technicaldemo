package com.amaris.demo.api.controller;

import static com.amaris.demo.exception.PriceExceptionHandler.INVALID_REQUEST_PARAMS_CODE;
import static com.amaris.demo.exception.PriceExceptionHandler.PRICE_NOT_FOUND_ERROR_CODE;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.amaris.demo.controller.PriceController;
import com.amaris.demo.domain.ErrorResponse;
import com.amaris.demo.domain.ProductPrice;
import java.time.LocalDateTime;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PriceControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testGetPriceWhenTwoPricesInSamePeriodShouldReturnWithMaxPriority() {
        String productId = "35455";
        String brandId = "1";
        String date = "2020-06-15T16:00:01";
        String request = getUrl(productId, brandId, date);

        ResponseEntity<ProductPrice> response = restTemplate.getForEntity(request, ProductPrice.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testGetPriceWhenNotFound() {
        String productId = "35455";
        String brandId = "2";
        String date = "2020-06-15T16:00:01";
        String request = getUrl(productId, brandId, date);

        ResponseEntity<ErrorResponse> response = restTemplate.getForEntity(request, ErrorResponse.class);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertEquals(PRICE_NOT_FOUND_ERROR_CODE, response.getBody().getCode());
    }

    @Test
    public void testGetPriceWhenInvalidParameterFormat() {
        String productId = "35455";
        String brandId = "2";
        String date = "2020-06-15T16:00:0A";
        String request = getUrl(productId, brandId, date);

        ResponseEntity<ErrorResponse> response = restTemplate.getForEntity(request, ErrorResponse.class);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals(INVALID_REQUEST_PARAMS_CODE, response.getBody().getCode());
    }

    @Test
    public void testGetPriceWhenParamConstraintViolation() {
        String productId = "-35455";
        String brandId = "2";
        String date = "2020-06-15T16:00:01";
        String request = getUrl(productId, brandId, date);

        ResponseEntity<ErrorResponse> response = restTemplate.getForEntity(request, ErrorResponse.class);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals(INVALID_REQUEST_PARAMS_CODE, response.getBody().getCode());
    }

    @ParameterizedTest
    @CsvSource({"2020-06-14T10:00:00, 1, 2020-06-14T00:00:00, 2020-12-31T23:59:59, 35.50",
        "2020-06-14T16:00:00, 2, 2020-06-14T15:00:00, 2020-06-14T18:30:00, 25.45",
        "2020-06-14T21:00:00, 1, 2020-06-14T00:00:00, 2020-12-31T23:59:59, 35.50",
        "2020-06-15T10:00:00, 3, 2020-06-15T00:00:00, 2020-06-15T11:00:00, 30.50",
        "2020-06-16T16:00:00, 4, 2020-06-15T16:00:00, 2020-12-31T23:59:59, 38.95"})
    public void testGetPriceOnDifferentDates(String date, Long priceRateId, LocalDateTime startDate, LocalDateTime endDate,
        String price) {
        String productId = "35455";
        String brandId = "1";
        String currency = "EUR";
        String request = getUrl(productId, brandId, date);

        ResponseEntity<ProductPrice> response = restTemplate.getForEntity(request, ProductPrice.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(Long.parseLong(productId), response.getBody().getProductId());
        assertEquals(priceRateId, response.getBody().getPriceRateId());
        assertEquals(Long.parseLong(brandId), response.getBody().getBrandId());
        assertEquals(startDate, response.getBody().getStartDate());
        assertEquals(endDate, response.getBody().getEndDate());
        assertEquals(price, response.getBody().getPrice());
        assertEquals(currency, response.getBody().getCurrency());
    }

    private String getUrl(String productId, String brandId, String date) {
        return PriceController.PRICE_RESOURCE + String.format("?productId=%s&brandId=%s&date=%s", productId, brandId, date);
    }

}
